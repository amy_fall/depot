import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.BorderLayout;






import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;






import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenDisplayUser extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel;
	private JLabel labelLogin ;
	private JTextField textFieldLogin ;
	private JButton boutonDelete ;
	JTextArea zoneTextListUser;
	private JButton boutonBack ;
	private UtilisateurDAO monUtilisateurDAO;
  
	

public FenDisplayUser(){
	
	monUtilisateurDAO = new UtilisateurDAO();
	
	this.setTitle("Delete User");
	this.setSize(800,200);
	this.setLocationRelativeTo(null);
	labelLogin=new JLabel("Login :");
    zoneTextListUser = new JTextArea(5, 20); 
    zoneTextListUser.setEditable(false);  
	containerPanel = new JPanel();
    containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
    containerPanel.setBackground(Color.WHITE);
    containerPanel.setVisible(true);
    textFieldLogin = new JTextField();
	boutonDelete = new JButton("Display User");
	boutonBack = new JButton("Back");
	containerPanel.add(labelLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(textFieldLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(boutonDelete);
	containerPanel.add(zoneTextListUser);
	containerPanel.add(boutonBack);

	
	boutonDelete.addActionListener(this);
	boutonBack.addActionListener(this);
	//permet de quitter l'application si on ferme la fen�tre
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
	this.setContentPane(containerPanel);
 
	//affichage de la fen�tre
	this.setVisible(true);
    
}

public void actionPerformed(ActionEvent ae123)
{
	if(ae123.getSource()==boutonDelete)
	{
		// on demande � la classe ArticleDAO d'ajouter le message
		// dans la base de donn�es
		Utilisateur amy = monUtilisateurDAO.getUtilisateur(this.textFieldLogin.getText());
	
		//on affiche dans la console du client les articles re�us
		
			 zoneTextListUser.append(amy.toString());
		
	
	}
	
	if(ae123.getSource()==boutonBack)
	{
		this.dispose();
		new FenAuthentification();
	}
	
	}

}
