import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 * Classe d'acc�s aux donn�es contenues dans la table Administrateur
 * @author FALL Amy
 * @version 1.1
 * */
public class AdministrateurDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle
	 * URL, LOGIN et PASS sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN="amfa";
	final static String PASS="bdd7";


	/**
	 * Constructeur de la classe
	 * 
	 */
	public AdministrateurDAO()
	{
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	

	/**
	 * Permet de r�cup�rer un administrateur � partir de son login et de son mot de passe
	 * @param reference la r�f�rence de l'article � r�cup�rer
	 * @return l'article
	 * @return null si aucun article ne correspond � cette r�f�rence
	 */
	public Administrateur connectionAdministrateur(String admin_login, String admin_password)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Administrateur retour=null;

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Administrateur WHERE (ADMIN_LOGIN = ? AND ADMIN_PASSWORD = ?)");
			ps.setString(1,admin_login);
			ps.setString(2, admin_password);

			//on ex�cute la requ�te
			//rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
			rs=ps.executeQuery();
			//passe � la premi�re (et unique) ligne retourn�e 
			if(rs.next())
				retour =new Administrateur(rs.getString("admin_login"),rs.getString("admin_password"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
}
