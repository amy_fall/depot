import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es contenues dans la table Utilisateur
 * @author FALL
 * @version 1.1
 * */
public class UtilisateurDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle
	 * URL, LOGIN et PASS sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN="amfa";
	final static String PASS="bdd7";


	/**
	 * Constructeur de la classe
	 * 
	 */
	public UtilisateurDAO()
	{
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	

	/**
	 * Permet d'ajouter un utilisateur dans la table Utilisateur
	 * @param nouvUtilisateur l'utilisateur � ajouter
	 * @return le nombre de lignes ajout�es dans la table
	 */
		//fermeture du preparedStatement et de la connexion
		public int addUser(Utilisateur nouvUtilisateur)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur � communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s de nouvUtilisateur
			ps = con.prepareStatement("INSERT INTO Utilisateur (user_login, user_password, user_nom, user_prenom) VALUES (?, ?, ?, ?)");
			ps.setString(1,nouvUtilisateur.getUserLogin());
			ps.setString(2,nouvUtilisateur.getUserPassword());
			ps.setString(3,nouvUtilisateur.getUserNom());
			ps.setString(4,nouvUtilisateur.getUserPrenom());

			//Ex�cution de la requ�te
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer un article � partir de sa r�f�rence
	 * @param reference la r�f�rence de l'article � r�cup�rer
	 * @return l'article
	 * @return null si aucun article ne correspond � cette r�f�rence
	 */
	
	public Utilisateur getUtilisateur(String user_login)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Utilisateur retour=null;

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Utilisateur WHERE user_login = ?");
			ps.setString(1,user_login);

			//on ex�cute la requ�te
			//rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
			rs=ps.executeQuery();
			//passe � la premi�re (et unique) ligne retourn�e 
			if(rs.next())
				retour=new Utilisateur(rs.getString("user_login"),rs.getString("user_password"),rs.getString("user_nom"),rs.getString("user_prenom"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	
	/**
	 * Permet de r�cup�rer tous les utilisateurs stock�s dans la table Utilisateur
	 * @return une ArrayList d'utilisateurs
	 */
	public List<Utilisateur> getListeUtilisateurs()
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		List<Utilisateur> retour=new ArrayList<Utilisateur>();

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Utilisateur");

			//on ex�cute la requ�te
			rs=ps.executeQuery();
			//on parcourt les lignes du r�sultat
			while(rs.next())
				retour.add(new Utilisateur(rs.getString("user_login"),rs.getString("user_password"),rs.getString("user_nom"),rs.getString("user_prenom")));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du rs, du preparedStatement et de la connexion
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	
	
	public Utilisateur connectionUtilisateur(String user_login, String user_password)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Utilisateur retour=null;

		//connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Utilisateur WHERE (USER_LOGIN = ? AND USER_PASSWORD = ?)");
			ps.setString(1,user_login);
			ps.setString(2, user_password);

			//on ex�cute la requ�te
			//rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
			rs=ps.executeQuery();
			//passe � la premi�re (et unique) ligne retourn�e 
			if(rs.next())
				retour =new Utilisateur(rs.getString("user_login"),rs.getString("user_password"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	
	public int deleteUser(Utilisateur nouvUtilisateur)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur � communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s de nouvUtilisateur
			ps = con.prepareStatement("DELETE FROM Utilisateur WHERE USER_LOGIN = ?");
			ps.setString(1,nouvUtilisateur.getUserLogin());


			//Ex�cution de la requ�te
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	
	
	public int modifyNomUser(Utilisateur nouvUtilisateur)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur � communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s de nouvUtilisateur
			ps = con.prepareStatement("UPDATE Utilisateur SET user_nom = ? WHERE USER_LOGIN = ?");
			ps.setString(1,nouvUtilisateur.getUserNom());
			ps.setString(2,nouvUtilisateur.getUserLogin());


			//Ex�cution de la requ�te
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	
	
	public int modifyPrenomUser(Utilisateur nouvUtilisateur)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur � communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s de nouvUtilisateur
			ps = con.prepareStatement("UPDATE Utilisateur SET user_prenom = ? WHERE USER_LOGIN = ?");
			ps.setString(1,nouvUtilisateur.getUserPrenom());
			ps.setString(2,nouvUtilisateur.getUserLogin());


			//Ex�cution de la requ�te
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}

	


}
