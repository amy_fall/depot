import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.BorderLayout;






import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;






import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenAdminSauthentifie extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel;
	private JLabel labelLogin ;
	private JLabel labelPassword ;
	private JTextField textFieldLogin ;
	private JPasswordField textFieldPassword ;
	private JButton boutonConnection ;
	private JButton boutonBack ;
	private Box boxFin ;
  
	

public FenAdminSauthentifie(){
	this.setTitle("Authentification Administrateur");
	this.setSize(400,200);
	this.setLocationRelativeTo(null);
	boxFin = Box.createHorizontalBox();
	labelLogin=new JLabel("Login :");
	labelPassword=new JLabel("Password :");
	containerPanel = new JPanel();
    containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
    containerPanel.setBackground(Color.GRAY);
    containerPanel.setVisible(true);
    textFieldLogin = new JTextField();
    textFieldPassword = new JPasswordField();
	boutonConnection = new JButton("Connection");
	boutonBack = new JButton("Back");
	containerPanel.add(labelLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(textFieldLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelPassword);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(textFieldPassword);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	boxFin.add(boutonConnection);
	boxFin.add(boutonBack);
	containerPanel.add(boxFin, BorderLayout.EAST);
	
	boutonConnection.addActionListener(this);
	boutonBack.addActionListener(this);
	//permet de quitter l'application si on ferme la fen�tre
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
	this.setContentPane(containerPanel);
 
	//affichage de la fen�tre
	this.setVisible(true);
    
}

public void actionPerformed(ActionEvent ae123)
{
	int retour;
	
	try {
		if(ae123.getSource()==boutonConnection)
		{
			Administrateur admin ;
			
			AdministrateurDAO monAdmin = new AdministrateurDAO(); 
			
			admin = monAdmin.connectionAdministrateur(this.textFieldLogin.getText(),this.textFieldPassword.getText());
			
			if(admin == null)
				System.out.println(" Cet administrateur n'existe pas ");
			else
				{
			System.out.println(" cet administrateur existe ");	
			
			this.setVisible(false);
			FenPortailAdministrateur sodffli = new FenPortailAdministrateur();
				}
		}
		
		if(ae123.getSource()==boutonBack)
		{
			this.setVisible(false);
			FenAuthentification dhjfkld = new FenAuthentification() ;
		}
		
	}
	catch (Exception e) {
		System.err.println("Veuillez contr�ler vos saisies");
	}
	
}

}
