
public class Administrateur {
	private String admin_login ; 
	private String admin_password ;
	private String admin_nom ;
	private String admin_prenom ;
	
	public Administrateur(String admin_login, String admin_password){
		this.admin_login = admin_login ;
		this.admin_password = admin_password ;
	}
	
	public Administrateur(String admin_login, String admin_password, String admin_nom, String admin_prenom){
		this.admin_login = admin_login ;
		this.admin_password = admin_password ;
		this.admin_nom = admin_nom ;
		this.admin_prenom = admin_prenom ;
	}

	
	public String getAdminLogin() {
		return admin_login;
	}
	
	public String getAdminPassword() {
		return admin_password ;
	}
	
	public String getAdminNom() {
		return admin_nom;
	}
	
	public String getAdminPrenom() {
		return admin_prenom;
	}
	
	public void setAdminLogin(String admin_login) {
		this.admin_login = admin_login ;
	}
	
	public void setAdminPassword(String admin_password){
		this.admin_password = admin_password ;
	}

	public void setAdminNom (String admin_nom){
		this.admin_nom = admin_nom ;
	}
	
	public void setAdminPrenom (String admin_prenom){
		this.admin_prenom = admin_prenom ;
	}
	

}
