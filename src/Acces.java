
public class Acces {
private String admin_login_acces ;
private String user_login_acces ;
private String accord_acces ;
private String heure_acces ;
private String sens_acces ;
 

 	
 	public Acces(String admin_login_acces, String user_login_acces, String accord_acces, String heure_acces, String sens_acces){
 		this.admin_login_acces = admin_login_acces ;
 		this.user_login_acces = user_login_acces ;
 		this.accord_acces = accord_acces ;
 		this.heure_acces = heure_acces ;
 		this.sens_acces = sens_acces ;
 	}
 	

 	
 	public String getAdminAccesLogin(){
 		return admin_login_acces ;
 	}
 	
 	public String getUserAccesLogin(){
 		return user_login_acces ;
 	}
 	
 
 	public String getAccordAcces(){
 		return accord_acces ;
 	}
 
 	public String getHeureAcces(){
 		return heure_acces ;
 	}
 
 	public String getSensAcces(){
 		return sens_acces ;
 	}
 	
 	public void setAdminAcces(Administrateur admin_acces){
 		this.admin_login_acces = admin_login_acces ;
 	}
 	
 	public void setUserAcces(Utilisateur user){
 		this.user_login_acces = user_login_acces ;
 	}
 
 	public void setAccordAcces(String accord_acces){
 		this.accord_acces = accord_acces ;
 	}
 
 	public void setHeureAcces(String heure_acces){
 		this.heure_acces = heure_acces ;
 	}
 
 	public void setSensAcces (String sens_acces){
 		this.sens_acces = sens_acces ;
 	}
 	
 	
 	public String toStringAcces() {
 		String statement_acces ;
 		if(accord_acces == "OUI"){
 			statement_acces = sens_acces+ " autoris�e � " +user_login_acces+ " � " +heure_acces+ " par " +admin_login_acces+ "." ;
 		}
 		else
 		{
 			statement_acces = sens_acces+ " non autoris�e � " +user_login_acces+ " � " +heure_acces+ " par " +admin_login_acces+ "." ;
 		}
 		return statement_acces ;
 	}
}
