import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.BorderLayout;





import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;





import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenModifyUserPrenom extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel;
	private JLabel labelLogin ;
	private JTextField textFieldLogin ;
	private JButton boutonModify ;
	private JButton boutonBack ;
	private Box boxFin ;
	private UtilisateurDAO monUser;
	private JTextField textfieldnom ;
	private JLabel labelnom ;
	

public FenModifyUserPrenom(){
	
	monUser = new UtilisateurDAO();
	
	this.setTitle("Delete User");
	this.setSize(800,200);
	this.setLocationRelativeTo(null);
	boxFin = Box.createHorizontalBox();
	labelLogin=new JLabel("Login :");
	labelnom =new JLabel("Nouveau pr�nom :");
	containerPanel = new JPanel();
    containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
    containerPanel.setBackground(Color.GRAY);
    containerPanel.setVisible(true);
    textFieldLogin = new JTextField();
    textfieldnom = new JTextField();
	boutonModify = new JButton("Modify");
	boutonBack = new JButton("Back");
	containerPanel.add(labelLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(textFieldLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelnom);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(textfieldnom);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));

	boxFin.add(boutonModify);
	boxFin.add(boutonBack);
	containerPanel.add(boxFin, BorderLayout.EAST);
	
	boutonModify.addActionListener(this);
	boutonBack.addActionListener(this);
	//permet de quitter l'application si on ferme la fen�tre
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
	this.setContentPane(containerPanel);
 
	//affichage de la fen�tre
	this.setVisible(true);
    
}

public void actionPerformed(ActionEvent ae123)
{
	int retour;
	
	try {
		if(ae123.getSource()==boutonModify)
		{
			
			
			this.dispose();
			
			Utilisateur u = new Utilisateur(this.textFieldLogin.getText(),"aze","aze", this.textfieldnom.getText());
			retour = monUser.modifyPrenomUser(u);
			System.out.println(""+ retour + "ligne modifi�e ");
			
			new FenRetourModifUser();
	
				
		}
		
		if(ae123.getSource()==boutonBack)
		{
			this.setVisible(false);
			new FenPortailAdministrateur() ;
		}
		
	}
	catch (Exception e) {
		System.err.println("Veuillez contr�ler vos saisies");
	}
	
}

}
