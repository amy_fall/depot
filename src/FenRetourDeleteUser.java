import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;





public class FenRetourDeleteUser extends JFrame implements ActionListener
{
	
	private static final long serialVersionUID = 1L; 
	
	private JPanel containerPanel;	
	
	private JLabel labelPhrase ;
	
	private JButton boutonDelete ;	 	
	
	private JButton boutonBack;	
	

	
	
	public FenRetourDeleteUser()
    {
		
		
		//on fixe le titre de la fen�tre
		this.setTitle("B�timents");
		//initialisation de la taille de la fen�tre
		this.setSize(800,200);
		this.setLocationRelativeTo(null);
		
		//cr�ation du conteneur
		containerPanel = new JPanel();
		
		//choix du Layout pour ce conteneur
		//il permet de g�rer la position des �l�ments
		//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
		//BoxLayout permet par exemple de positionner les �lements sur une colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		
		//choix de la couleur pour le conteneur
        containerPanel.setBackground(Color.GRAY);
        
        
		//instantiation des  composants graphiques
		boutonDelete=new JButton("Delete User");
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		boutonBack = new JButton("BACK");
		labelPhrase = new JLabel("Utilisateur supprim� avec succ�s !");
   
		//ajout des composants sur le container 
		containerPanel.add(labelPhrase);
		//introduire une espace constant entre le label et le bouton
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(boutonDelete);
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(boutonBack);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));		
		//ajouter une bordure vide de taille constante autour de l'ensemble des composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		//ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonDelete.addActionListener(this);
		boutonBack.addActionListener(this);
		
		//permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setContentPane(containerPanel);

		//affichage de la fen�tre
		this.setVisible(true);
	}
	
	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent aeamy)
	{
		int retour;
		
		try {
			if(aeamy.getSource()==boutonDelete)
			{
				this.setVisible(false);
				new FenDeleteUser() ;
			}
			
			if(aeamy.getSource()==boutonBack)
			{
				this.setVisible(false);
				new FenPortailAdministrateur() ;
			}
			
		}
		catch (Exception e) {
			System.err.println("Veuillez contr�ler vos saisies");
		}
		
	}

	
	
}


