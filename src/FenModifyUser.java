import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;





import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenModifyUser extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel ;
	private JButton boutonModifyNom ;
	private JButton boutonModifyPrenom ;
	private JButton boutonModifyPassword ;
	private JButton boutonBack ;
	
	
	public FenModifyUser(){
		this.setTitle("Portail Administrateur");
		this.setSize(600,300);
		this.setLocationRelativeTo(null);
		this.setLayout(new GridLayout(1, 6));
		containerPanel = new JPanel();
	    containerPanel.setLayout(new GridLayout(10,12));
	    containerPanel.setBackground(Color.GRAY);
	    containerPanel.setVisible(true);
	    boutonModifyNom = new JButton("Modify Name");
	    boutonModifyPrenom = new JButton("Modify Prenom");
	    boutonModifyPassword = new JButton("Modify Password");
	    boutonBack = new JButton("Back");
	    containerPanel.add(boutonModifyNom);
	    containerPanel.add(boutonModifyPrenom);
	    containerPanel.add(boutonModifyPassword);
	    containerPanel.add(boutonBack);
	    
	    boutonModifyNom.addActionListener(this);
	    boutonModifyPrenom.addActionListener(this);
	    boutonModifyPassword.addActionListener(this);
	    boutonBack.addActionListener(this);
	    
		//permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(containerPanel);
	    this.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent ae12345)
	{
		int retour;
		
		try {
			if(ae12345.getSource()==boutonModifyNom)
			{
				this.dispose();
				new FenModifyUserNom();
			}
			
			if(ae12345.getSource()==boutonModifyPrenom)
			{
				this.dispose();
				new FenModifyUserPrenom();
				
			}
			
			if(ae12345.getSource()==boutonModifyPassword)
			{
				this.dispose();
				
			}
			
			if(ae12345.getSource()==boutonBack)
			{
				this.dispose();
				
			}
			 			
			
			
			
		}
		catch (Exception e) {
			System.err.println("Veuillez contr�ler vos saisies");
		}
		
	}
	
}
