import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


public class FenDisplayAccessList extends JFrame implements ActionListener
{
	
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel;		
	private JButton boutonBack ;
	JTextArea zoneTextListUser;
	JScrollPane zoneDefilement;
	private AccesDAO tonAccesDAO;
	
	
	public FenDisplayAccessList()
    {
		// on instancie la classe Article DAO
		this.tonAccesDAO = new AccesDAO();
		
		//on fixe le titre de la fen�tre
		this.setTitle("Display Access List");
		//initialisation de la taille de la fen�tre
		this.setSize(800,200);
		
		//cr�ation du conteneur
		containerPanel = new JPanel();
		boutonBack = new JButton("Back");
		
		//choix du Layout pour ce conteneur
		//il permet de g�rer la position des �l�ments
		//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
		//BoxLayout permet par exemple de positionner les �lements sur une colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		
		//choix de la couleur pour le conteneur
        containerPanel.setBackground(Color.PINK);
        
        
		//instantiation des  composants graphiques

        zoneTextListUser = new JTextArea(5, 20);
        zoneDefilement = new JScrollPane(zoneTextListUser); 
        zoneTextListUser.setEditable(false);     
       
		
		
		
		containerPanel.add(zoneDefilement);
		containerPanel.add(boutonBack);
		//ajouter une bordure vide de taille constante autour de l'ensemble des composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
	
		boutonBack.addActionListener(this);
		int retour;
		
		// on demande � la classe ArticleDAO d'ajouter le message
		// dans la base de donn�es
		List<Acces> liste = tonAccesDAO.getListeAcces();
	
		//on affiche dans la console du client les articles re�us
		for(Acces b : liste)
		{
			 zoneTextListUser.append(b.toStringAcces());
		     zoneTextListUser.append("\n");
			//Pour afficher dans la console : System.out.println(a.toString());	
		}
		//permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setContentPane(containerPanel);

		//affichage de la fen�tre
		this.setVisible(true);
    }
	
	public void actionPerformed(ActionEvent ae)
	{
		int retour;
		
		try {
			if(ae.getSource()==boutonBack)
			{
				this.dispose();
				new FenPortailAdministrateur();
			}
			
			
			
			
			
		}
		catch (Exception e) {
			System.err.println("Veuillez contr�ler vos saisies");
		}
		
	}
	

}
