import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;





import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenPortailAdministrateur extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel ;
	private JButton boutonDisplayAccessList ;
	private JButton boutonDisplayUserList ;
	private JButton boutonDisplayAccessUser ;
	private JButton boutonModifyUser ;
	private JButton boutonDeleteUser ;
	private JButton boutonAddUser ;
	private JButton boutonDeconnection ;
	private JButton boutonSetAccess ;
	
	public FenPortailAdministrateur(){
		this.setTitle("Portail Administrateur");
		this.setSize(600,300);
		this.setLocationRelativeTo(null);
		this.setLayout(new GridLayout(1, 6));
		containerPanel = new JPanel();
	    containerPanel.setLayout(new GridLayout(10,12));
	    containerPanel.setBackground(Color.GRAY);
	    containerPanel.setVisible(true);
	    boutonDisplayAccessList = new JButton("Display Full Access List");
	    boutonDisplayUserList = new JButton("Display User List");
	    boutonDisplayAccessUser = new JButton("Display Access/User ");
	    boutonSetAccess = new JButton("Set Access");
	    boutonModifyUser = new JButton("Modify User");
	    boutonDeleteUser = new JButton("Delete User");
	    boutonAddUser = new JButton("Add User");
	    boutonDeconnection = new JButton("Deconnection");
	    containerPanel.add(boutonSetAccess);
	    containerPanel.add(boutonDisplayAccessList);
	    containerPanel.add(boutonDisplayUserList);
	    containerPanel.add(boutonDisplayAccessUser);
	    containerPanel.add(boutonModifyUser);
	    containerPanel.add(boutonDeleteUser);
	    containerPanel.add(boutonAddUser);
	    containerPanel.add(boutonDeconnection);
	    
	    boutonSetAccess.addActionListener(this);
	    boutonAddUser.addActionListener(this);
	    boutonDisplayUserList.addActionListener(this);
	    boutonDeleteUser.addActionListener(this);
	    boutonDeconnection.addActionListener(this);
	    boutonModifyUser.addActionListener(this);
	    boutonDisplayAccessList.addActionListener(this);
	    boutonDisplayAccessUser.addActionListener(this);
	    
		//permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(containerPanel);
	    this.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent ae12345)
	{
		int retour;
		
		try {
			
			if(ae12345.getSource()==boutonSetAccess)
			{
				this.dispose();
				new FenSetAccess();
			}
			
			
			if(ae12345.getSource()==boutonAddUser)
			{
				this.dispose();
				new FenetreAddUser();
			}
			
			if(ae12345.getSource()==boutonDisplayUserList)
			{
				this.dispose();
				new FenDisplayUserList() ;
			}
			
			if(ae12345.getSource()==boutonDeleteUser)
			{
				this.dispose();
				new FenDeleteUser() ;
			}
			
			if(ae12345.getSource()==boutonDeconnection)
			{
				this.dispose();
				new FenAuthentification() ;
			}
			
			if(ae12345.getSource()==boutonModifyUser)
			{
				this.dispose();
				new FenModifyUser() ;
			}
			
			if(ae12345.getSource()==boutonDisplayAccessList)
			{
				this.dispose();
				new FenDisplayAccessList() ;
			}
			
			if(ae12345.getSource()==boutonDisplayAccessUser)
			{
				this.dispose();
				new FenDisplayAccessUser() ;
			}
			
			
			
			
		}
		catch (Exception e) {
			System.err.println("Veuillez contr�ler vos saisies");
		}
		
	}
	
}
