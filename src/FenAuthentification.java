import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;




public class FenAuthentification extends JFrame implements ActionListener
{
	
	private static final long serialVersionUID = 1L;
	
	private JPanel containerPanel;	
	
	private JLabel labelAccueil ;
	
	private JButton boutonUtilisateur ;	 	
	
	private JButton boutonAdministrateur;	
	
	private AdministrateurDAO monAdministrateurDAO;
	
	
	public FenAuthentification()
    {
		// on instancie la classe Article DAO
		this.monAdministrateurDAO = new AdministrateurDAO();
		
		//on fixe le titre de la fen�tre
		this.setTitle("PLATEFORME D'AUTHENTIFICATION");
		//initialisation de la taille de la fen�tre
		this.setSize(800,500);
		this.setLocationRelativeTo(null);
		
		//cr�ation du conteneur
		containerPanel = new JPanel( );
		
		//choix du Layout pour ce conteneur
		//il permet de g�rer la position des �l�ments
		//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
		//BoxLayout permet par exemple de positionner les �lements sur une colonne ( PAGE_AXIS )
		containerPanel.setLayout(new FlowLayout());
		
		//choix de la couleur pour le conteneur
        containerPanel.setBackground(Color.YELLOW);
        
        
		//instantiation des  composants graphiques
		boutonAdministrateur=new JButton("ADMINISTRATEUR");
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		boutonUtilisateur = new JButton("UTILISATEUR");
		labelAccueil = new JLabel("ACCUEIL");
   
		//ajout des composants sur le container 
		containerPanel.add(labelAccueil);
		//introduire une espace constant entre le label et le bouton
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(boutonUtilisateur);
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(boutonAdministrateur);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));		
		//ajouter une bordure vide de taille constante autour de l'ensemble des composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		//ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonUtilisateur.addActionListener(this);
		boutonAdministrateur.addActionListener(this);
		
		//permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setContentPane(containerPanel);

		//affichage de la fen�tre
		this.setVisible(true);
	}
	
	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae)
	{
		int retour;
		
		try {
			if(ae.getSource()==boutonAdministrateur)
			{
				this.setVisible(false);
				FenAdminSauthentifie soli = new FenAdminSauthentifie();
			}
			
			else
			{
				this.setVisible(false);
				FenUserSauthentifie ok = new FenUserSauthentifie();
			}
			
		}
		catch (Exception e) {
			System.err.println("Veuillez contr�ler vos saisies");
		}
		
	}

	
	
}

