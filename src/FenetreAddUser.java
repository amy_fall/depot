import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;









import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenetreAddUser extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel;
	private JLabel labelLogin ;
	private JLabel labelPassword ;
	private JLabel labelNom ;
	private JLabel labelPrenom ;
	private JTextField textFieldLogin ;
	private JPasswordField textFieldPassword ;
	private JTextField textFieldNom ;
	private JTextField textFieldPrenom ;
	private JButton boutonAdd ;
	private JButton boutonBack ;
	private Box boxFin ;
	private UtilisateurDAO monUtilisateurDAO;
  
	

public FenetreAddUser(){
	this.monUtilisateurDAO = new UtilisateurDAO() ;
	this.setTitle("Add User");
	//this.setResizable(false);
	this.setSize(400,400);
	this.setLocationRelativeTo(null);
	boxFin = Box.createHorizontalBox();
	labelLogin=new JLabel(" Login :  ");
	labelPassword=new JLabel("    Password :       ");
	labelNom=new JLabel(" Nom :");
	labelPrenom=new JLabel(" Prenom :");
	containerPanel = new JPanel();
    containerPanel.setLayout(new BorderLayout());
    containerPanel.setBackground(Color.WHITE);
    containerPanel.setVisible(true);
    textFieldLogin = new JTextField();
    textFieldPassword = new JPasswordField();
    textFieldNom = new JTextField();
    textFieldPrenom = new JTextField();
	boutonAdd = new JButton("Add     ");
	boutonBack = new JButton("Back   ");
	containerPanel.add("West" , labelLogin);
	containerPanel.add("East" , textFieldLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(10,10)));
	containerPanel.add("West", labelPassword);
	containerPanel.add("East",textFieldPassword);
	//containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelNom);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldNom);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelPrenom);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldPrenom);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	boxFin.add(boutonAdd);
	boxFin.add(boutonBack);
	containerPanel.add(boxFin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	
	boutonAdd.addActionListener(this);
	
	//permet de quitter l'application si on ferme la fen�tre
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
			
	this.setContentPane(containerPanel);

	//affichage de la fen�tre
	this.setVisible(true);
}
   
	public void actionPerformed(ActionEvent aeadduser)
	{
		int retour;
		
		try {
			if(aeadduser.getSource()==boutonAdd)
			{
				//on cr�e l'objet message
				Utilisateur usera = new Utilisateur(this.textFieldLogin.getText(),this.textFieldPassword.getText(),this.textFieldNom.getText(),this.textFieldPrenom.getText());
				//on demande � la classe de communication d'envoyer l'article dans la table article
				retour = monUtilisateurDAO.addUser(usera);
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " utilisateur ajout�e ");
				this.setVisible(false);
				FenRetourAddUser liki = new FenRetourAddUser() ;
			}
			
			else
			{
				this.setVisible(false);
				FenPortailAdministrateur kflk = new FenPortailAdministrateur();
			}
			
		}
		catch (Exception e) {
			System.err.println("Veuillez contr�ler vos saisies");
		}
		
	}
    
        
}
