import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;






import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenUserSauthentifie extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel;
	private JLabel labelLogin ;
	private JLabel labelPassword ;
	private JTextField textFieldLogin ;
	private JPasswordField textFieldPassword ;
	private JButton boutonConnection ;
	private JButton boutonBack ;
  
	

public FenUserSauthentifie(){
	this.setTitle("Authentification Utilisateur");
	this.setSize(800,200);
	this.setLocationRelativeTo(null);
	labelLogin=new JLabel("Login :");
	labelPassword=new JLabel("Password :");
	containerPanel = new JPanel();
    containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
    containerPanel.setBackground(Color.GRAY);
    containerPanel.setVisible(true);
    textFieldLogin = new JTextField();
    textFieldPassword = new JPasswordField();
	boutonConnection = new JButton("CONNECTION");
	boutonBack = new JButton("BACK");
	containerPanel.add(labelLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelPassword);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldPassword);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(boutonConnection);
	containerPanel.add(boutonBack);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	
	boutonConnection.addActionListener(this);
	boutonBack.addActionListener(this);
	
	//permet de quitter l'application si on ferme la fen�tre
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
	this.setContentPane(containerPanel);

	//affichage de la fen�tre
	this.setVisible(true);   
        
}

public void actionPerformed(ActionEvent ae1234)
{	
int retour;
	
	try {
		if(ae1234.getSource()==boutonConnection)
		{
			Utilisateur user ;
			
			UtilisateurDAO monUser = new UtilisateurDAO(); 
			
			user = monUser.connectionUtilisateur(this.textFieldLogin.getText(),this.textFieldPassword.getText());
			
			if(user == null)
				{
				System.out.println(" Cet utilisateur n'existe pas ");
			
				this.dispose();
				new FenUserSauthentifie() ;
				}
			else
				{
			System.out.println(" Cet utilisateur existe ");	
			
			this.dispose();
			new FenDisplayAccessUser();
				}
		}
		
		if(ae1234.getSource()==boutonBack)
		{
			this.dispose();
			new FenAuthentification() ;
		}
		
	}
	catch (Exception e) {
		System.err.println("Veuillez contr�ler vos saisies");
	}
	
}
}
