
public class Utilisateur {
	private String user_login ; 
	private String user_password ;
	private String user_nom ;
	private String user_prenom ;
	
	public Utilisateur(String user_login, String user_password){
		this.user_login = user_login ;
		this.user_password = user_password ;
	}
	
	public Utilisateur(String user_login, String user_nom, String user_prenom){
		this.user_login = user_login ;
		this.user_password = "null" ;
		this.user_nom = user_nom ;
		this.user_prenom = user_prenom ;
	}
	
	public Utilisateur(String user_login, String user_password, String user_nom, String user_prenom){
		this.user_login = user_login ;
		this.user_password = user_password ;
		this.user_nom = user_nom ;
		this.user_prenom = user_prenom ;
	}
	
	public String getUserLogin() {
		return user_login;
	}
	
	public String getUserPassword() {
		return user_password;
	}
	
	public String getUserNom() {
		return user_nom;
	}
	
	public String getUserPrenom() {
		return user_prenom;
	}
	
	public void setUserLogin(String user_login) {
		this.user_login = user_login ;
	}
	
	public void setUserPassword(String user_password) {
		this.user_password = user_password ;
	}
	
	public void setUserNom (String user_nom){
		this.user_nom = user_nom ;
	}
	
	public void setUserPrenom (String user_prenom){
		this.user_prenom = user_prenom ;
	}
	
	public String toString() {
		return "User [ " +user_login+ " - " +user_nom+ " " + user_prenom+ "]";
	}

}
