import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.BorderLayout;






import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;






import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenDisplayAccessAdmin extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel;
	private JLabel labelLogin ;
	private JTextField textFieldLogin ;
	private JButton boutonDelete ;
	JTextArea zoneTextListUser;
	JScrollPane zoneDefilement;
	private JButton boutonBack ;
	private AccesDAO monAccesDAO;
  
	

public FenDisplayAccessAdmin(){
	
	monAccesDAO = new AccesDAO();
	
	this.setTitle("Delete User");
	this.setSize(800,200);
	this.setLocationRelativeTo(null);
	labelLogin=new JLabel("Login :");
    zoneTextListUser = new JTextArea(5, 20);
    zoneDefilement = new JScrollPane(zoneTextListUser); 
    zoneTextListUser.setEditable(false);  
	containerPanel = new JPanel();
    containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
    containerPanel.setBackground(Color.GRAY);
    containerPanel.setVisible(true);
    textFieldLogin = new JTextField();
	boutonDelete = new JButton("Display Access List");
	boutonBack = new JButton("Back");
	containerPanel.add(labelLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(textFieldLogin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(boutonDelete);
	containerPanel.add(zoneDefilement);
	containerPanel.add(boutonBack);

	
	boutonDelete.addActionListener(this);
	boutonBack.addActionListener(this);
	//permet de quitter l'application si on ferme la fen�tre
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
	this.setContentPane(containerPanel);
 
	//affichage de la fen�tre
	this.setVisible(true);
    
}

public void actionPerformed(ActionEvent ae123)
{
	if(ae123.getSource()==boutonDelete)
	{
		// on demande � la classe ArticleDAO d'ajouter le message
		// dans la base de donn�es
		List<Acces> liste = monAccesDAO.getListeAccesAdmin(this.textFieldLogin.getText());
	
		//on affiche dans la console du client les articles re�us
		for(Acces a : liste)
		{
			 zoneTextListUser.append(a.toStringAcces());
		     zoneTextListUser.append("\n");	
		}
	
	}
	
	if(ae123.getSource()==boutonBack)
	{
		this.dispose();
		new FenAuthentification();
	}
	
	}

}
