import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es contenues dans la table Utilisateur
 * @author FALL
 * @version 1.1
 * */
public class AccesDAO {

	/**
	 * Param�tres de connexion � la base de donn�es oracle
	 * URL, LOGIN et PASS sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN="amfa";
	final static String PASS="bdd7";


	/**
	 * Constructeur de la classe
	 * 
	 */
	public AccesDAO()
	{
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	

	/**
	 * Permet d'ajouter un utilisateur dans la table Utilisateur
	 * @param nouvUtilisateur l'utilisateur � ajouter
	 * @return le nombre de lignes ajout�es dans la table
	 */
		//fermeture du preparedStatement et de la connexion
		public int setAccess(Acces nouvAcces)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion � la base de donn�es
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			//pr�paration de l'instruction SQL, chaque ? repr�sente une valeur � communiquer dans l'insertion
			//les getters permettent de r�cup�rer les valeurs des attributs souhait�s de nouvUtilisateur
			ps = con.prepareStatement("INSERT INTO Acces (admin_login_acces, user_login_acces, accord_acces, heure_acces, sens_acces) VALUES (?, ?, ?, ?, ?)");
			ps.setString(1,nouvAcces.getAdminAccesLogin());
			ps.setString(2,nouvAcces.getUserAccesLogin());
			ps.setString(3,nouvAcces.getAccordAcces());
			ps.setString(4,nouvAcces.getHeureAcces());
			ps.setString(5,nouvAcces.getSensAcces());

			//Ex�cution de la requ�te
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
		
		
		public Acces getAcces()
		{

			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs=null;
			Acces retour=null;

			//connexion � la base de donn�es
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM Acces ");

				//on ex�cute la requ�te
				//rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
				rs=ps.executeQuery();
				//passe � la premi�re (et unique) ligne retourn�e 
				if(rs.next())
					retour=new Acces(rs.getString("admin_login_acces"),rs.getString("user_login_acces"),rs.getString("accord_acces"),rs.getString("heure_acces"),rs.getString("sens_acces"));


			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				//fermeture du ResultSet, du PreparedStatement et de la Connection
				try {if (rs != null)rs.close();} catch (Exception t) {}
				try {if (ps != null)ps.close();} catch (Exception t) {}
				try {if (con != null)con.close();} catch (Exception t) {}
			}
			return retour;

		}
		
		/**
		 * Permet de r�cup�rer tous les utilisateurs stock�s dans la table Utilisateur
		 * @return une ArrayList d'utilisateurs
		 */
		public List<Acces> getListeAccesLogin(String user_login_acces)
		{

			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs=null;
			List<Acces> retour1=new ArrayList<Acces>();

			//connexion � la base de donn�es
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM Acces WHERE user_login_acces = ?");
				ps.setString(1,user_login_acces);

				//on ex�cute la requ�te
				rs=ps.executeQuery();
				//on parcourt les lignes du r�sultat
				while(rs.next())
					retour1.add(new Acces(rs.getString("admin_login_acces"),rs.getString("user_login_acces"),rs.getString("accord_acces"),rs.getString("heure_acces"),rs.getString("sens_acces")));


			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				//fermeture du rs, du preparedStatement et de la connexion
				try {if (rs != null)rs.close();} catch (Exception t) {}
				try {if (ps != null)ps.close();} catch (Exception t) {}
				try {if (con != null)con.close();} catch (Exception t) {}
			}
			return retour1;
		}
		
		public List<Acces> getListeAccesAdmin(String admin_login_acces)
		{

			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs=null;
			List<Acces> retour2=new ArrayList<Acces>();

			//connexion � la base de donn�es
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM Acces WHERE admin_login_acces = ?");
				ps.setString(1,admin_login_acces);

				//on ex�cute la requ�te
				rs=ps.executeQuery();
				//on parcourt les lignes du r�sultat
				while(rs.next())
					retour2.add(new Acces(rs.getString("admin_login_acces"),rs.getString("user_login_acces"),rs.getString("accord_acces"),rs.getString("heure_acces"),rs.getString("sens_acces")));


			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				//fermeture du rs, du preparedStatement et de la connexion
				try {if (rs != null)rs.close();} catch (Exception t) {}
				try {if (ps != null)ps.close();} catch (Exception t) {}
				try {if (con != null)con.close();} catch (Exception t) {}
			}
			return retour2;
		}
			public List<Acces> getListeAcces()
			{

				Connection con = null;
				PreparedStatement ps = null;
				ResultSet rs=null;
				List<Acces> retour=new ArrayList<Acces>();

				//connexion � la base de donn�es
				try {

					con = DriverManager.getConnection(URL, LOGIN, PASS);
					ps = con.prepareStatement("SELECT * FROM Acces");

					//on ex�cute la requ�te
					rs=ps.executeQuery();
					//on parcourt les lignes du r�sultat
					while(rs.next())
						retour.add(new Acces(rs.getString("admin_login_acces"),rs.getString("user_login_acces"),rs.getString("accord_acces"),rs.getString("heure_acces"),rs.getString("sens_acces")));


				} catch (Exception ee) {
					ee.printStackTrace();
				} finally {
					//fermeture du rs, du preparedStatement et de la connexion
					try {if (rs != null)rs.close();} catch (Exception t) {}
					try {if (ps != null)ps.close();} catch (Exception t) {}
					try {if (con != null)con.close();} catch (Exception t) {}
				}
				return retour;

		}


}
