import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;





import javax.swing.JFrame;

import java.awt.Graphics;

import javax.swing.JPanel;
 
       

public class FenSetAccess extends JFrame implements ActionListener {      
	private static final long serialVersionUID = 1L; 
	private JPanel containerPanel;
	private JLabel labelAdmin ;
	private JLabel labelUser ;
	private JLabel labelAccord ;
	private JLabel labelHeure ;
	private JLabel labelSens ;
	private JTextField textFieldAdmin ;
	private JTextField textFieldUser ;
	private JTextField textFieldAccord ;
	private JTextField textFieldHeure ;
	private JTextField textFieldSens ;
	private JButton boutonAdd ;
	private JButton boutonBack ;
	private Box boxFin ;
	private AccesDAO monAccesDAO;
  
	

public FenSetAccess(){
	this.monAccesDAO = new AccesDAO() ;
	this.setTitle("Add User");
	this.setSize(800,200);
	this.setLocationRelativeTo(null);
	boxFin = Box.createHorizontalBox();
	labelAdmin=new JLabel("Admin Login :");
	labelUser=new JLabel("User Login :");
	labelAccord=new JLabel("Accord :");
	labelHeure=new JLabel("Heure :");
	labelSens=new JLabel("Sens :");
	containerPanel = new JPanel();
    containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
    containerPanel.setBackground(Color.GRAY);
    containerPanel.setVisible(true);
    textFieldAdmin = new JTextField();
    textFieldUser = new JTextField();
    textFieldAccord = new JTextField();
    textFieldHeure = new JTextField();
    textFieldSens = new JTextField();
	boutonAdd = new JButton("Add");
	boutonBack = new JButton("Back");
	containerPanel.add(labelAdmin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldAdmin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelUser);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldUser);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelAccord);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldAccord);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelHeure);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldHeure);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	containerPanel.add(labelSens);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	containerPanel.add(textFieldSens);
	containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
	boxFin.add(boutonAdd);
	boxFin.add(boutonBack);
	containerPanel.add(boxFin);
	containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
	
	boutonAdd.addActionListener(this);
	
	//permet de quitter l'application si on ferme la fen�tre
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
			
	this.setContentPane(containerPanel);

	//affichage de la fen�tre
	this.setVisible(true);
}
   
	public void actionPerformed(ActionEvent aeadduser)
	{
		int retour;
		
		try {
			if(aeadduser.getSource()==boutonAdd)
			{
				//on cr�e l'objet message
				Acces usera = new Acces(this.textFieldAdmin.getText(),this.textFieldUser.getText(),this.textFieldAccord.getText(),this.textFieldHeure.getText(),this.textFieldSens.getText());
				//on demande � la classe de communication d'envoyer l'article dans la table article
				retour = monAccesDAO.setAccess(usera);
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " acc�s ajout� ");
				this.dispose();
				new FenSetAccess() ;
			}
			
			else
			{
				this.dispose();
				new FenPortailAdministrateur();
			}
			
		}
		catch (Exception e) {
			System.err.println("Veuillez contr�ler vos saisies");
		}
		
	}
    
        
}
